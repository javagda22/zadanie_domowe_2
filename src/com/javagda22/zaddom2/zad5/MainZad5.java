package com.javagda22.zaddom2.zad5;

import java.util.Scanner;

public class MainZad5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę: ");
        int liczba = scanner.nextInt();

        // wartość bezwzględna:
        if (liczba > 0) {
//            liczba = liczba;
            System.out.println(liczba);
        } else {
            liczba = -liczba;
            System.out.println(liczba);
        }
        // skorzystać z biblioteki Java
        liczba = Math.abs(liczba);

        // wartość przeciwna
        liczba = -liczba;

        // przeciwna liczba (Math)
        liczba = Math.negateExact(liczba);

        // wartość odwrotna
        double odwrotna = 1.0 / liczba;
        System.out.println(odwrotna);
    }
}
