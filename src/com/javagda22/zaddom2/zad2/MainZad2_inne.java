package com.javagda22.zaddom2.zad2;

import java.util.Arrays;
import java.util.Random;

public class MainZad2_inne {
    public static void main(String[] args) {
        Random generatorLiczbLosowych = new Random();

        int[] tablica = new int[20];
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = generatorLiczbLosowych.nextInt(10) + 1;
        }
        System.out.println(Arrays.toString(tablica));

        int[] zliczenia = new int[11]; // 0, 1, 2, ... 10
        for (int i = 0; i < tablica.length; i++) {
            int liczba = tablica[i];

            zliczenia[liczba]++;
        }
        // tablica -> 1 1 2 3 3 3 5 9
        // zliczenia .
        // 0
        // 1 + +
        // 2 +
        // 3 + + +
        // 4
        // 5 +
        // 6
        for (int i = 0; i < zliczenia.length; i++) {
            System.out.println(i + " -> " + zliczenia[i]);
        }
    }
}
