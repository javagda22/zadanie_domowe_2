package com.javagda22.zaddom2.zad2;

import java.util.Random;

public class MainZad2 {
    public static void main(String[] args) {
        Random generatorLiczbLosowych = new Random();

        int[] tablica = new int[20];
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = generatorLiczbLosowych.nextInt(10) + 1;
        }

        for (int sprLiczba = 1; sprLiczba < 11; sprLiczba++) {
            int sumaZliczen = 0;

            for (int i = 0; i < tablica.length; i++) {
                if (tablica[i] == sprLiczba) {
                    sumaZliczen++;
                }
            }

            System.out.println(sprLiczba + " -> " + sumaZliczen);
        }

    }
}
