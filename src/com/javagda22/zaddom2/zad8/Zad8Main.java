package com.javagda22.zaddom2.zad8;

import java.util.Scanner;

public class Zad8Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String imie = scanner.next();

        if (imie.equals("Anna")) {
            System.out.println("Siema Ania!");
        } else if (imie.equals("Cezary")) {
            System.out.println("Yo yo Cezary");
        } else if (imie.equals("Jonasz")) {
            System.out.println("Elo elo Jonasz!");
        }else{
            System.out.println("Nic");
        }
        ///
        /// switch - poniższy switch zachowuje się dokładnie tak, jak if u góry
        switch (imie) {
            case "Anna":
                System.out.println("Siema Ania!");
                break;
            case "Cezary":
                System.out.println("Yo yo Cezary");
                break;
            case "Jonasz":
                System.out.println("Elo elo Jonasz!");
                break;
            default:
                System.out.println("Nic");
                break;
        }
    }
}
