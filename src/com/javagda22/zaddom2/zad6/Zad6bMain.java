package com.javagda22.zaddom2.zad6;

import java.util.Scanner;

public class Zad6bMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj rok: ");
        double rok = scanner.nextInt();

        System.out.println("Podaj miesiac: ");
        double miesiac = scanner.nextInt();

        System.out.println(rok + miesiac);
        System.out.println(rok - miesiac);
        System.out.println(rok * miesiac);
        System.out.println(rok / miesiac);
        System.out.println(rok % miesiac);
    }
}
