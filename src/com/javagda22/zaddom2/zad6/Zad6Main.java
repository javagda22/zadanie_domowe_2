package com.javagda22.zaddom2.zad6;

import java.util.Scanner;

public class Zad6Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj rok: ");
        int rok = scanner.nextInt();

        System.out.println("Podaj miesiac: ");
        int miesiac = scanner.nextInt();

        System.out.println(rok + miesiac);
        System.out.println(rok - miesiac);
        System.out.println(rok * miesiac);
        System.out.println(rok / miesiac);
        System.out.println(rok % miesiac);
    }
}
