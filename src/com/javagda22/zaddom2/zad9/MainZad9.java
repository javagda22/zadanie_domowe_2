package com.javagda22.zaddom2.zad9;

public class MainZad9 {
    public static void main(String[] args) {
        /// a - for
        for (int i = -200; i >= -1000; i--) {
            System.out.println(i);
        }
        /// a - while
        int iterator = -200;
        while (iterator >= -1000) {
            System.out.println(iterator--);

//            iterator--;
        }
    }
}
