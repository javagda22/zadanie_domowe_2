package com.javagda22.zaddom2.zad9;

import java.util.Scanner;

public class MainZad9c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // można przyjąć ze scanner'a
        int rozmiarY = scanner.nextInt();
        int rozmiarX = scanner.nextInt();

        for (int x = 1; x <= rozmiarX; x++) {
            for (int y = 1; y <= rozmiarY; y++) {
                System.out.print(x * y + "  ");
            }
            System.out.println();
        }

//        for (int x = 0; x < rozmiarX; x++) {
//            for (int y = 0; y < rozmiarY; y++) {
//                System.out.print((x + 1) * (y + 1) + "  ");
//            }
//            System.out.println();
//        }
    }
}
