package com.javagda22.zaddom2.zad9;

public class MainZad9b {
    public static void main(String[] args) {
        /// a - for
        for (int i = 1000; i < 100000; i++) {
            System.out.print(i + ", ");
            if (i % 1000 == 0) {
                System.out.println();
            }
        }
        /// a - while
        int iterator = 1000;
        while (iterator < 100000) {
            System.out.print(iterator + ", ");

            if (iterator % 1000 == 0) {
                System.out.println();
            }
            iterator++;
        }
    }
}
