package com.javagda22.zaddom2.zad1;

import java.util.Arrays;
import java.util.Random;

public class MainZad1 {
    public static void main(String[] args) {
        // konieczne jest podanie rozmiaru tablicy przy jej deklaracji
        int[] tablica = new int[10];

        Random generator = new Random();
        for (int i = 0; i < tablica.length; i++) { // pętla do 10
            // generowanie liczby z zakresu 0-20 a następnie odjęcie 10 przesuwa przedziął o 10
            tablica[i] = generator.nextInt(21) - 10; // liczby od -10  --  10
        }

        // 1.b
        // oddzielna pętla dla wypisania
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + ", ");
        }
        System.out.println();

        // 1.c
        // wyznaczyć największy i najmniejszy
        int max = tablica[0], min = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            // jeśli element jest większy od maximum, to max = element
            if (tablica[i] > max) {
                max = tablica[i];
            }
            // jeśli element jest mniejszy od min, to min = element
            if (tablica[i] < min) {
                min = tablica[i];
            }
        }
        System.out.println("Min: " + min);
        System.out.println("Max: " + max);

        // 1.d - średnia arytmetyczna

        double suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            // sumuje wszystkie wartości
            suma += tablica[i];
        }
        System.out.println("Suma: " + suma);
        double srednia = suma / tablica.length;
        System.out.println("Srednia: " + srednia);

        // 1.e
        int mniejsze = 0;
        int wieksze = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > srednia) {
                wieksze++; // wieksze = wieksze + 1;
            }
            if (tablica[i] < srednia) {
                mniejsze++;
            }
        }
        System.out.println("Mniejsze: " + mniejsze);
        System.out.println("Wieksze: " + wieksze);

        // 1.f
        for (int i = tablica.length - 1; i >= 0; i--) {
            System.out.println(tablica[i]);
        }

        // 1.g
        Arrays.sort(tablica);

        System.out.println("Posortowana:");
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + ", ");
        }
        System.out.println(Arrays.toString(tablica));

        if (tablica.length % 2 == 0) {
            // parzysta liczba elementów
            int srodkowy_1 = tablica.length / 2; // 10 - 5  [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            int srodkowy_2 = (tablica.length / 2) - 1; // 10 - 5  [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]\

            double sumaElementow = tablica[srodkowy_1] + tablica[srodkowy_2];
            System.out.println("Mediana: " + (sumaElementow / 2));
        } else {
            // mediana to srodkowy element jesli dlugosc tablicy jest nieparzysta.
            System.out.println("Mediana: " + tablica[tablica.length / 2]);
        }
    }
}
