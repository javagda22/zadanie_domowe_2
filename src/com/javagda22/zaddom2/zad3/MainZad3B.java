package com.javagda22.zaddom2.zad3;

import java.util.Arrays;
import java.util.Scanner;

public class MainZad3B {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj początek zakresu:");
        int poczatekZakresu = scanner.nextInt();

        System.out.println("Podaj koniec zakresu:");
        int koniecZakresu = scanner.nextInt();

        if (poczatekZakresu < koniecZakresu) {
            // ok!
            // 3.b
            System.out.println("Podaj ilość dzielników: ");
            int ilośćDzielników = scanner.nextInt();
            int[] dzielniki = new int[ilośćDzielników];
            for (int i = 0; i < ilośćDzielników; i++) {
                System.out.println("Podaj dzielnik nr. " + i + ".:");
                dzielniki[i] = scanner.nextInt();
            }

            // sprawdza dzielniki
            for (int i = poczatekZakresu; i <= koniecZakresu; i++) {

                boolean podzielna = true;
                for (int iteratorDzielnikow = 0; iteratorDzielnikow < ilośćDzielników; iteratorDzielnikow++) {
                    podzielna = podzielna && (i % dzielniki[iteratorDzielnikow] == 0);
                }
                if(podzielna) {
                    System.out.println("Liczba " + i + " jest podzielna przez wszystkie dzielniki " + Arrays.toString(dzielniki));
                }
            }

        } else {
            System.out.println("Podałeś/łaś złe wartości!");
        }
    }
}
