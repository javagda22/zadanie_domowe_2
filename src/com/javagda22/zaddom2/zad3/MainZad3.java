package com.javagda22.zaddom2.zad3;

import java.util.Scanner;

public class MainZad3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj początek zakresu:");
        int poczatekZakresu = scanner.nextInt();

        System.out.println("Podaj koniec zakresu:");
        int koniecZakresu = scanner.nextInt();

        if (poczatekZakresu < koniecZakresu) {
            // ok!
            // 3.a
            System.out.println("Podaj dzielnik: ");
            int dzielnik = scanner.nextInt();

            for (int i = poczatekZakresu; i <= koniecZakresu; i++) {
                if (i % dzielnik == 0) {
                    System.out.println("Liczba " + i + " jest podzielna przez " + dzielnik);
                }
            }

        } else {
            System.out.println("Podałeś/łaś złe wartości!");
        }
    }
}
