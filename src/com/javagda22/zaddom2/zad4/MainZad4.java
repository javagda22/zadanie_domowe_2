package com.javagda22.zaddom2.zad4;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MainZad4 {
    public static void main(String[] args) {
        //4.a
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę N:");
        int N = scanner.nextInt();

        // bez użycia tablicy
        Random generator = new Random();
        for (int i = 0; i < N; i++) {
            System.out.print(generator.nextInt() + ", ");
        }

        // z użyciem tablicy
        int[] tablica = new int[N];
        for (int i = 0; i < N; i++) {
            // losujemy liczbę z przedziału do +/- int
            tablica[i] = generator.nextInt();
        }

        System.out.println(Arrays.toString(tablica));
    }
}
